#pragma once

#include <vector>
#include <list>
#include <algorithm>
#include <ostream>
#include <string>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <stack>
#include <ctime>
#include<set> 
#include <queue>


namespace graph {
	enum class color { BLACK, GRAY, WHITE };

	class vertex;

	typedef std::shared_ptr<vertex> vertex_ptr;

	class edge {
	public:
		edge(vertex_ptr adjacent_vertex, int wt) :
			vptr_(std::move(adjacent_vertex)), cost_(wt)
		{}

		edge()
		{}

		vertex_ptr& get_vertex() {
			return vptr_;
		}

		int get_cost() {
			return cost_;
		}

	private:
		int cost_;
		vertex_ptr vptr_;
	};

	typedef std::shared_ptr<edge> edge_ptr;

	class vertex {
	public:
		vertex(std::string id, color cl = color::WHITE, int dist = 0) :
			name_(id), color_(cl), dist_(dist)
		{}

		// empty constructor to create empty obj
		/*vertex()
		{}
		*/
		~vertex() {}

		typedef std::shared_ptr<vertex> vertex_ptr;

		// add edge
		bool connect_edge(vertex_ptr adjacent, int wt);

		// get name of the node
		const std::string& get_id() const {
			return name_;
		}

		// this will change as the vertex are discovered
		void change_state(color cl) {
			color_ = cl; 
		}

		// query the color state
		color get_state() {
			return color_;
		}

		// get the list of edges connected to this vertex
		const std::list<edge_ptr>& get_edges() const {
			return edges_;
		}

		// put the edges in PQ for MST implementation
		void edge_pq() {
			//auto cmp = [](edge_ptr& a, edge_ptr& b) { return a->get_cost() > b->get_cost(); };

			//std::priority_queue<int, edge_ptr, decltype(cmp)> pq(cmp);
			// smallest edge first based on cost
			for (auto& edge : edges_) {
				q_edges_lowest_first_.push(edge);
			}
		}

	private:
		// private helpers
		//// compare two edges by weight
		int compareEdges(edge_ptr E, edge_ptr F) {
			return E->get_cost() < F->get_cost();
		}

		// 
		std::string name_;
		int dist_;
		color color_;
		vertex_ptr ancestor;
		std::time_t start_time;
		std::time_t end_time;

		std::mutex mtx_; // use to protect vertex object
		std::list<edge_ptr> edges_; // should this be std::set so no dup checks?

		// lets have a priority queue of edges in decending order of cost
		std::priority_queue<int, edge_ptr, decltype(compareEdges)> q_edges_lowest_first_;
	};

	//template<class T> using std::shared_ptr<graph::vertex<T>> vertex_ptr;
	//template<class T> using vertex_ptr = typename vertex<T>::unique_ptr_type;
	//edge
	// template<class T> using edge_ptr = std::unique_ptr<edge<T>>;

	

	enum class graph_type {UNDIRECTED, DIRECTED};

	class graph {
	public:
		graph()
		{}

		// insert a pair of cities, with distance between them
		bool insert(std::string& Vert1, std::string& Vert2, int wt, graph_type gt = graph_type::UNDIRECTED);
		
		// Breath-First search
		void BFS(const std::string& startingVertex);

		// Depth-First search
		void DFS(const std::string& startingVertex);

		// Topological sort
		void Topological_Sort(const std::string& startingVertex);

		// Shortest-Path using Dijkstra greedy algo
		void DijkstraSP(std::string& startingVertex);
		
		// Transpose this graph for connected component
		std::shared_ptr<graph> Transpose(void);

	private:
		std::list<vertex_ptr> vertices_;

		std::mutex mtx_; // use this lock to protect graph object
		// use std::map to store the smart ptr and name (key)
		std::map<std::string, vertex_ptr> map_nameVptr;
		graph_type type_; // DAG, cyclic, etc

		// following is for DijkstraSP implementation
		// this should be abstracted out - will not work
		// after we make this async()
		uint64_t dsp_path_length_;
		uint64_t dsp_path_cost_;

		// helpers
		bool BFSAlgorithm(vertex_ptr vptr);
		bool DFSAlgorithm(vertex_ptr vptr);
		bool TopoSorter(vertex_ptr vptr, std::stack<vertex_ptr>&);
		bool DijkstraSPHelper(vertex_ptr vptr);

		// push all the vertices to a set
		void populate_vertex_set(std::set<vertex_ptr>&);
	};

	// shared ptr for graph obj
	typedef std::shared_ptr<graph> graph_ptr;

} // namespace
// Graphs.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Graphs.h"
#include <tuple>



int main()
{
	graph::graph G;
	std::initializer_list<std::string> cities({ "San Francisco", "LA",
	"San Francisco", "Baltimore", "New York", "New Delhi", "Bombay", "Dallas",
	"Chicago", "Toronto", "London", "Cairo", "Ceylon", "Shanghai", "Alhambra",
	"Baghdad", "Moscow", "Paris", "Frankfurt", "Berlin", "Cape Town" });

	std::vector<std::tuple<std::string, std::string, int>> CityPDist = { std::make_tuple("San Francisco", "LA", 240),
	std::make_tuple("LA", "San Francisco", 240), std::make_tuple("San Francisco", "Baltimore", 2500), 
	std::make_tuple("Baltimore", "New York", 400),  std::make_tuple("Dallas", "New York", 1200),
	std::make_tuple("Chicago", "New York", 900), std::make_tuple("New York", "Chicago", 400),
	std::make_tuple("Seattle", "New York", 400), std::make_tuple("Chicago", "Seattle", 400) };

	/*
	std::cout << "Enter the name of the first city";
	std::string cityFrom;
	std::cin >> cityFrom;

	std::cout << std::endl;

	std::cout << "Enter the name of the second city"; 
	std::string cityTo;
	std::cin >> cityTo;

	int dist;
	std::cout << "Enter the distance between " << cityFrom << " and "
		<< cityTo;
	std::cin >> dist;
	*/

	for (auto& city : CityPDist) {
		G.insert(std::get<0>(city), std::get<1>(city), std::get<2>(city));
	}

	G.BFS("San Francisco");

    return 0;
}


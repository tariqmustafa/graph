#include "stdafx.h"
#include "Graphs.h"
#include <assert.h>

//using namespace graph;

bool
graph::vertex::connect_edge(vertex_ptr vptr, int cost)
{
	// grab the smart lock
	std::unique_lock<std::mutex> lock(mtx_);

	// search if the vptr already exists in the list
	auto iter = std::find_if(edges_.begin(), edges_.end(), [&](const edge_ptr &edge) {
		//elem->get_vertex().get() == vptr.get(); 
		return this->get_id() == edge->get_vertex()->get_id();
	});

	if (iter == edges_.end()) {
		// not found - insert in the list
		edges_.push_back(std::make_shared<edge>(vptr, cost));
	}

	return true;
}


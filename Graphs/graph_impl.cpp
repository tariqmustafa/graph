#include "stdafx.h"
#include "Graphs.h"
#include <assert.h>
#include <queue>
#include <deque>
#include <sstream>      // std::ostringstream
//#include <stack>

bool
graph::graph::insert(std::string& Vert1, std::string& Vert2, int cost, graph_type gt)
{
	vertex_ptr vptr1, vptr2;

	// check in the map if the vertexs exist
	auto itr = map_nameVptr.find(Vert1);
	if (itr == map_nameVptr.end()) {
		vptr1 = std::make_shared<vertex>(Vert1);
		assert(vptr1 != nullptr);

		vertices_.push_back(vptr1);
		// insert in the map
		map_nameVptr.emplace(std::make_pair(Vert1, vptr1));
	}
	else { // this vertex is already in the graph
		vptr1 = itr->second;  // ?? std::move for unique
	}

	auto itr1 = map_nameVptr.find(Vert2);
	if (itr1 == map_nameVptr.end()) {
		vptr2 = std::make_shared<vertex>(Vert2);
		assert(vptr2 != nullptr);

		vertices_.push_back(vptr2);
		map_nameVptr.emplace(std::make_pair(Vert2, vptr2));
	}
	else {
		vptr2 = itr1->second;
	}

	// add this edge to the vertex1
	vptr1->connect_edge(vptr2, cost);
	

	if (gt == graph_type::UNDIRECTED) {
		// TODO:
	}

	return true;
}

void
graph::graph::BFS(const std::string& VertexName)
{
	// find if the Vertex exists
	std::unique_lock<std::mutex> lock(mtx_);

	auto iter = map_nameVptr.find(VertexName);
	if (iter == map_nameVptr.end()) {
		std::cout << "Starting vertex (" << VertexName 
			<< ") does not exist. Please choose another node, and try again." 
			<< std::endl;
		return;
	}

	vertex_ptr vptr = iter->second;

	bool ret = BFSAlgorithm(vptr);
}

void graph::graph::DFS(const std::string & startingVertex)
{
	std::unique_lock<std::mutex> lock(mtx_);

	auto iter = map_nameVptr.find(startingVertex);
	if (iter == map_nameVptr.end()) {
		std::cout << "Starting vertex (" << startingVertex
			<< ") does not exist. Please choose another node, and try again."
			<< std::endl;
		return;
	}

	vertex_ptr vptr = iter->second;

	if (vptr->get_state() == color::WHITE) {
		bool ret = DFSAlgorithm(vptr);
		if (!ret) {
			std::cout << "Something didn't went quite right" << std::endl;
		}
		return;
	}
	else {
		std::cout << "Vertex already discovered" << std::endl;
	}
}

/**
Algorithm of Topological sort

Algorithm implemented here takes O(V+E) - linear run time. Uses the
stack to implement the sort.

Ref: https://courses.cs.washington.edu/courses/cse326/03wi/lectures/RaoLect20.pdf
*/
void graph::graph::Topological_Sort(const std::string & startingVertex)
{
	std::stack<vertex_ptr> toposort;

	std::unique_lock<std::mutex> lock(mtx_);

	auto iter = map_nameVptr.find(startingVertex);
	if (iter == map_nameVptr.end()) {
		return;
	}

	vertex_ptr vptr = iter->second;

	if (vptr->get_state() == color::WHITE) {
		TopoSorter(vptr, toposort);
	}

	std::ostringstream str;
	str << "Dumping the toposorted list" << std::endl;
	for (size_t i = 0; i <= toposort.size(); i++) {  // why is 'auto' treating this as int? // incorrect parameter deduction??
		str << toposort.top() << " ";
		toposort.pop();
	}
	str << std::endl;

	std::cout << str.str();
}

bool
graph::graph::BFSAlgorithm(vertex_ptr vptr)
{
	assert(vptr);

	std::cout << "BFS Algorithm starting from node: " << vptr->get_id()
		<< std::endl;

	// put this node in the queue
	 std::queue<vertex_ptr> qofneighbors;

	// changed to deque to be able to use the range based for loop
	// which does not work with queue, as its a container that
	// does not have begin()
	//std::deque<vertex_ptr> qofneighbors;
	qofneighbors.push(vptr);

	vptr->change_state(color::GRAY);

	//for (auto vertex : qofneighbors) {
	while ( !qofneighbors.empty()) {
		vertex_ptr vertex = qofneighbors.front();
		qofneighbors.pop();

		//vertex_ptr v = vertex;
		std::list<edge_ptr> edges = vertex->get_edges();
		// iterate the edge list of the vertex
		for (auto& edge : edges) {
			std::cout << "Discovered: " << edge->get_vertex()->get_id() << "--->";

			color cl = edge->get_vertex()->get_state();
			if (cl == color::GRAY || cl == color::BLACK) {
				std::cout << "Rediscovered vertex: " << vertex->get_id() << "skipping...";
				continue; // this has already been discovered
			}

			qofneighbors.push(edge->get_vertex());

			// change the state to partially discovered
			edge->get_vertex()->change_state(color::GRAY);
		}

		// set the vertex state of completely discovered (all edges)
		vertex->change_state(color::BLACK);
	}

	return true;
}

bool graph::graph::DFSAlgorithm(vertex_ptr vptr)
{
	vptr->change_state(color::GRAY);

	for (auto& edge : vptr->get_edges()) {
		if (edge->get_vertex()->get_state() == color::WHITE)
			DFSAlgorithm(edge->get_vertex());
	}

	vptr->change_state(color::BLACK);

	return true;
}

bool graph::graph::TopoSorter(vertex_ptr vptr, std::stack<vertex_ptr>& stk)
{
	vptr->change_state(color::GRAY);
	for (auto& edge : vptr->get_edges()) {
		if (edge->get_vertex()->get_state() == color::WHITE)
			TopoSorter(edge->get_vertex(), stk);
	}

	stk.push(vptr);

	return false;
}

graph::graph_ptr graph::graph::Transpose()
{
	/*
	The trick is simple, just pull out the vertex (say C) from the list of a vertex (say A),
	and put A in the list of edges for C.
	*/
	graph_ptr gtrans = std::make_shared<graph>();
	assert(gtrans != nullptr);

	std::unique_lock<std::mutex> lock(mtx_);

	// make copy of the vertex
	for (auto vert : vertices_) {
		// add this vertex to the transposed graph
		// vert is a shared ptr, no need to reinvent the wheel
		// vertex_ptr v = std::make_shared<vertex>(vert->get_id());
		gtrans->vertices_.push_back(vert);
		gtrans->map_nameVptr.emplace(std::make_pair(vert->get_id(), vert));

		// make copy of the edge
		for (auto edge : vert->get_edges()) {
			// insert vert to this edge 
			// before vert --> edge1->vertex, edge2->vertex, etc
			// transpose reverses the edge ordering, so now
			// edge1->vertex --> vert
			gtrans->map_nameVptr.emplace(std::make_pair(edge->get_vertex()->get_id(), edge->get_vertex()));
			edge->get_vertex()->connect_edge(vert, edge->get_cost());
		}
	}
	return gtrans;
}

